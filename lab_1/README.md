# Lab 1

## Please follow the following ROS Tutorials before your next Lab (07/09/18)

[Index of Tutorials](http://wiki.ros.org/ROS/Tutorials)

Start with [Starting with ROS](http://wiki.ros.org/ROS/Tutorials/InstallingandConfiguringROSEnvironment) up till [Using rosed](http://wiki.ros.org/ROS/Tutorials/UsingRosEd)  
**Complete Tutorials 1-9**