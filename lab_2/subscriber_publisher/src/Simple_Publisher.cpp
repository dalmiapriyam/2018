#include "ros/ros.h"
#include "std_msgs/String.h"

int main(int argc, char **argv) {
    // Initialize the node handler
    ros::init(argc, argv, "Simple_CPP_Publisher");
    // Create a node handler
    ros::NodeHandle nodeHandler;
    // Create a publishing object
    ros::Publisher publisherObject = nodeHandler.advertise<std_msgs::String>("CPP_String_Topic", 1000);
    // Let's publish at 5Hz (5 times a second)
    ros::Rate rateHandler(5);
    
    while(ros::ok()) {
        // Make a sample message and feed it with data
        std_msgs::String messageObject;
        char msg[] = "Simple C++ data";
        messageObject.data = msg;
        // Publish the message
        publisherObject.publish(messageObject);
        // Write info
        ROS_INFO("Published [%s]", msg);
        // Spin once
        ros::spinOnce();
        // Cause a delay and repeat
        rateHandler.sleep();
    }
    return 0;
}